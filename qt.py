from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QMainWindow, QPushButton, QApplication, QLabel



import sys

import number_of_ps


class FirstWindow(QWidget):


    def __init__(self, parent=None):
        super(FirstWindow,self).__init__(parent)

        header = "Systemanalyzer"

        introduction = "\nTool for retrieving system data (browser-history, running processes etc.)"

        os_text = "\n\n\nChoose your Operating System:"

        ## Heading
        #buttonReply = QMessageBox.question(self, 'PyQt5 message', "Do you like PyQt5?")
        self.header = QLabel(self)
        self.header.setText(header)
        self.header.move(25, 30)
        self.header.setStyleSheet("color: black; font: bold 25px")

        ## Introduction Text
        self.intro = QLabel(self)
        self.intro.setText(introduction)
        self.intro.move(25, 65)
        self.intro.setStyleSheet("color: black; font: bold 16px")

        ## OS Text
        self.os = QLabel(self)
        self.os.setText(os_text)
        self.os.move(25, 100)
        self.os.setStyleSheet("color: black; font: bold 16px")

        ## Pics
        
        # Tux
        lbl = QLabel(self)
        pixmap = QPixmap('Logos/tux.jpeg')
        maller_pixmap = pixmap.scaled(100, 100, Qt.KeepAspectRatio, Qt.FastTransformation)
        lbl.move(120, 220)
        lbl.setPixmap(maller_pixmap)
        lbl.show() 

        # Windows Logo
        lbl = QLabel(self)
        pixmap = QPixmap('Logos/win_logo.png')
        maller_pixmap = pixmap.scaled(100, 100, Qt.KeepAspectRatio, Qt.FastTransformation)
        lbl.move(350, 220)
        lbl.setPixmap(maller_pixmap)
        lbl.show()

        ## Linux Button
        button_linux = QPushButton('Linux', self)
        button_linux.move(130, 340)  # x - y Achse
        button_linux.setToolTip("Fenster schließen")
        button_linux.clicked.connect(self.newWindow)

        ## Windows Button
        button_windows = QPushButton('Windows', self)
        button_windows.move(360, 340)  # x - y Achse
        button_windows.setToolTip("Fenster schließen")
        button_windows.clicked.connect(self.newWindow)
        #self.connect(button_windows, SIGNAL('clicked()'), self.newWindow)

        ## Close-Button
        button = QPushButton('Close', self)
        button.move(250, 450)  # x - y Achse
        button.setToolTip("Fenster schließen")
        button.clicked.connect(self.close)

    ## open new window
    def newWindow(self):
        self.SW = SecondWindowLinux()
        #self.SW = SecondWindowLinux()
        #self.SW.append(dialog)
        self.SW.show()
        #pass

        # ## Execution Button
        # button = QPushButton('Test', self)
        # button.move(50, 50)
        # button.setToolTip("Execute")
        # #button.clicked.connect(myScript)
        # button.clicked.connect(number_of_ps.read_pids)

        # ## Input text field
        # self.textbox = QLineEdit(self)
        # self.textbox.move(50, 150)
        # self.textbox.resize(150,25)

        # ## Execution Button
        # button = QPushButton('Test', self)
        # button.move(210, 150)
        # button.setToolTip("Execute")
        # #button.clicked.connect(myScript)
        # button.clicked.connect(number_of_ps.read_pids)


    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Nachricht', "Soll die Anwendung geschlossen werden?",
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        # Variante 1
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


class SecondWindowLinux(QWidget):

    def __init__(self, parent=None):
        super(SecondWindowLinux, self).__init__(parent)
        # lbl = QLabel('Second Window', self)

        self.setGeometry(1000,1000,600,600)
        self.setWindowTitle('Analyse Step 2')

        header_2 = "Analyse Ihres LinuxOS Rechners"

        ## Heading
        #buttonReply = QMessageBox.question(self, 'PyQt5 message', "Do you like PyQt5?")
        self.header = QLabel(self)
        self.header.setText(header_2)
        self.header.move(25, 30)
        self.header.setStyleSheet("color: black; font: bold 25px")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWindow = FirstWindow()
    mainWindow.setGeometry(1000,1000,600,600)
    mainWindow.setWindowTitle('Systemanalyzer')
    mainWindow.setWindowIcon(QIcon("testicon.png"))
    mainWindow.show()
    #app.quit
    sys.exit(app.exec_())
