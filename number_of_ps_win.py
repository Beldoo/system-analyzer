'''
Check, how many processes are running
'''

import os
from termcolor import * # install via pip3
import colorama
import subprocess
import re
import time as t


def clean_tasklist():

    '''
    This function deletes all previous files with running windows processes.
    '''

    Dir = os.listdir()
    for ii in range(len(Dir)):
        browser_files = re.search('tasklist.txt', Dir[ii], flags=re.IGNORECASE)  # has URL in it
        if (browser_files != None):
            try:
                os.remove(str(Dir[ii]))
                print('\"{0}\" was removed.\n'.format(str(Dir[ii])))
            except Exception as err:
                print(err)
                pass


def get_processes_windows():

    '''
    This function lists all the running processes on windows host and saves it to 
    a .txt-file.
    '''
    try:
        subprocess.call('tasklist >> tasklist.txt', shell=True)
        print("Data collected successfully.")
    except Exception as e:
        print(e)


def list_processes_windows():

    '''
    This function prints all the processes saved previously.
    '''

    with open('tasklist.txt') as myfile:
            for line in myfile:
                print(line)


def number_of_ps_windows():

    '''
    This function counts the number of processes running on the windows machine.
    '''

    colorama.init()
    counter = 1
    with open('tasklist.txt') as myfile:
            for line in myfile:
                counter += 1
            cprint("Number of running processes: {0}".format(counter-2), 'blue', attrs=['bold'])


def find_name_of_pid_win(pid_of_interest):
    
    '''
    This function filters all the lines containing the process NAMES and associates PIDs, since there are
    many more lines per process conatining additional infos!
    '''

    colorama.init()
    name_pid_list = []

    f = open("tasklist.txt","r")

    for line in f:
        temp = line.split(" ")
        temp = filter(None, temp)
        c = 0
        for item in temp:
            #print(item)
            name_pid_list.append(item)
            c += 1
            if c > 1:
                break

    index = name_pid_list.index(str(pid_of_interest))
    cprint("{0} (PID: {1})".format(name_pid_list[index-1], pid_of_interest), 'blue')


def main():
    t.sleep(1)
    print("Begin {0}".format(__name__))
    t.sleep(1)
    clean_tasklist()
    t.sleep(1)
    get_processes_windows()
    t.sleep(1)
    # list_processes_windows()
    # t.sleep(1)
    # number_of_ps_windows()
    # t.sleep(1)
    find_name_of_pid_win(68)

if __name__ == '__main__':
    main()