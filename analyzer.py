'''
Main Program
'''

import os
import sys
from itertools import islice



def clean():

    '''
    Delete all files from directory containing file names that are going to be 
    created by the program itself.
    '''

    try:
        os.remove("ps.txt")
    except:
        pass

    try:
        os.remove("error.txt")
    except:
        pass

    try:
        os.remove("names.txt")
    except:
        pass
    
    try:
        os.remove("child_processes.txt")
    except:
        pass

    print("\nVerzeichnis sauber.\n")



def usage():
    print("\nPlease use one of the following flags to get valid results:\n")
    print("\t\"--number\" or \"-n\"\t\tDisplay number of currently runnung processes.\n")
    print("\t\"--list\" or \"-l\"\t\tDisplay list of running processes and associated process ID.\n")
    print("\t\"--find-pid\" or \"-f-pid\"\t\tFind a certain process ID and the associated process name.\n")
    print("\t\"--find-pname\" or \"-f-pname\"\t\tFind a certain process name and the associated process ID.\n")
    print("\t\"--browser-history\" or \"-b\"\t\tRetrieve browser history of the current user.\n")


def main():

    import check_procfs
    import create_error_file
    import create_error_file
    import number_of_ps
    import overview
    import browser

    if len(sys.argv) == 1:
        usage()
        sys.exit(0)

    clean()
    create_error_file.create_error_file()
    check_procfs.check_if_procfs_is_mounted()


    myrange = len(sys.argv)
    myrange = list(range(myrange-1))
    #print(myrange)
    z = iter(myrange)

    for ii in z:
        
        '''
        loop through all flags (argv[1]-argv[n]) and check if used flags exist
        '''
        
        if sys.argv[ii+1] == "--help" or sys.argv[ii+1] == "-h":
            usage()

        elif sys.argv[ii+1] == "--number" or sys.argv[ii+1] == "-n":
            number_of_ps.read_pids()
        
        elif sys.argv[ii+1] == "--list" or sys.argv[ii+1] == "-l":
            overview.gather_pids()
            overview.gather_all_infos()
            overview.output_pid_name()

        elif sys.argv[ii+1] == "--find-pid" or sys.argv[ii+1] == "-f-pid":
            overview.gather_pids()
            overview.gather_all_infos()
            overview.find_name_of_pid(sys.argv[ii+2]) # next argv is input for search function
            _ = list(islice(z, 1)) # die Schleife muss um ZWEI weiterspringen, weil die kommende argv der INPUT für die aktuelle Fkt. ist

        elif sys.argv[ii+1] == "--find-pname" or sys.argv[ii+1] == "-f-pname":
            overview.gather_pids()
            overview.gather_all_infos()
            overview.find_pid_of_psname(sys.argv[ii+2]) # next argv is input for search function
            _ = list(islice(z, 1)) # die Schleife muss um ZWEI weiterspringen, weil die kommende argv der INPUT für die aktuelle Fkt. ist

        elif sys.argv[ii+1] == "--find-children" or sys.argv[ii+1] == "-f-ch":
            overview.gather_pids()
            overview.gather_all_infos()
            overview.find_all_children_of_ps(sys.argv[ii+2]) # next argv is input for search function
            _ = list(islice(z, 1)) # die Schleife muss um ZWEI weiterspringen, weil die kommende argv der INPUT für die aktuelle Fkt. ist

        elif sys.argv[ii+1] == "--browser-history" or sys.argv[ii+1] == "-b":
            host_platform = browser.check_platform()
            if host_platform == 'linux':
                browser.browser_history_firefox_linux()


        else:
            print("\nCan't process \"{0}\"    For more information use --help or -h\n".format(sys.argv[ii+1]))


if __name__ == '__main__':
    main()