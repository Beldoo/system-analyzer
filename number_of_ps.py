'''
Check, how many processes are running
'''

import os
from termcolor import colored # install via pip3


def read_pids():

    processes = []
    other = []
    path = "/proc"

    for root, dirs, filenames in os.walk(path):
        counter = 1
        for item in dirs:
            #print(item)
            try:
                item = int(item)
                processes.append(item)
            except:
                other.append(item)    
        break
         
                
    #print("\nBeginne PIDs aufzuzählen:")
    # for p in processes:
    #     print(p) 
 
    # print("\nBeginne Rest aufzuzählen:")
    # for o in other:
    #     print(o)

    print("There are \033[01;31m{0}\033[00m running processes.".format(len(processes)))


def main():
    read_pids()


if __name__ == '__main__':
    main()