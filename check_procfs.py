'''
Check if procfs is mounted
'''

import subprocess
from termcolor import colored # install via pip3


def check_if_procfs_is_mounted():


        # check if system API to kernel is empty
        # 0: SUCCESS, >0: FAILURE

        if subprocess.call(["ls", "/proc"], stdout=subprocess.DEVNULL) != 0:
           
            colorred = "\033[01;31m{0}\033[00m" 
            print(colorred.format("++++++++++++++++++++++"))
            print(colorred.format("+++++++ Fehler +++++++"))
            print(colorred.format("++++++++++++++++++++++"))
            
            f = open("error.txt", "a")
            f.write("Problems with locating procfs")
            exit(1)

        else:
            #print("Procfs successfully located.")
            pass


def main():
        print("\n****************************\ncheck_procfs.py is running\n****************************\n")
        check_if_procfs_is_mounted()
        
if __name__ == '__main__':
        main()
