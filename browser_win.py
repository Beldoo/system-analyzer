import re
import sys
import os
from shutil import copy
import re
import sqlite3
import os.path

def clean_browser():

    Dir = os.listdir()
    for ii in range(len(Dir)):
        browser_files = re.search('URL|History|places.sqlite', Dir[ii], flags=re.IGNORECASE)  # has URL in it
        if (browser_files != None):
            try:
                os.remove(str(Dir[ii]))
                print('\"{0}\" was removed.\n'.format(str(Dir[ii])))
            except Exception as err:
                print(err)
                pass




def browser_history_firefox_windows():

    '''
    This function gets the user data (browser history) located in the home directory from Mozilla's Firefox Webbrowser.
    The Program must be installed within the home directory of the user...
    '''

    print("\n\n\n++++++++++++++++++++++++++++++++\n\n")
    cwd_path = os.getcwd() # the directory where the database shall be placed (original db is LOCKED, therefore it must be copied)
                           # into this folder
    print("Current Direcory: {0}".format(cwd_path))
    # get the directory-path where the browser data is stored
    home_folder = os.path.expanduser('~')
    #print("Home Folder: {0}".format(home_folder))
    path = home_folder + "\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles"
    os.chdir(path) 
    Dir = os.listdir()
    # C:\Users\IEUser\AppData\Local\Mozilla\Firefox\Profiles
        
    # ## retrieve the folder which contains 
    for ii in range(len(Dir)):
        print("DIR: {0}".format(Dir[ii]))

        path = home_folder + "\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles" # WICHTIG: Pfad muss immer wieder neu als Basis genommen werden

        data_file = re.search('.default', Dir[ii])  # ending can be ".default" or ".default-release"

        if (data_file != None):

            path += "\\" + str(Dir[ii]) + "\\places.sqlite"  # final path which contains the sqlite-db with the browser history
            print("Final Path: {0}".format(path))
            try:
                copy(path, cwd_path)  # must be copied into antother folder, because it is LOCKED within the .mozilla-folder
            except Exception as err:
                print("copy error:")
                print(err)

            # change the working-directory to where the unlocked sqlite-db is located now
            os.chdir(cwd_path)

            browser_history = []

            try:
                conn = sqlite3.connect(cwd_path + '\\places.sqlite')
                cursor = conn.cursor()
                print("Connection to database successfully.")
                # open file where all URLs will be stored in
                f = open("URLs_Firefox_Win.txt", "a")
                cursor.execute("SELECT * FROM moz_places")
                rows = cursor.fetchall()
                f.write('\n\nQuelle: {0}\n\n'.format(path))
                for row in rows:
                    browser_history.append(row[1])
                    f.write('\n' + row[1] + '\n')
                cursor.close()
                conn.close()
                f.close()
                print("Connection to database closed.")
            except Exception as err:
                print(err)


def browser_history_chrome_windows():

    '''
    This function gets the user data (browser history) located in the home directory from Google's Chrome Webbrowser.
    The Program must be installed within the home directory of the user...
    '''

    cwd_path = os.getcwd() # the directory where the database shall be placed (original db is LOCKED, therefore it must be copied)
                           # into this folder
    
    # get the directory-path where the browser data is stored
    home_folder = os.path.expanduser('~')

    path = home_folder + "\\AppData\Local\\Google\\Chrome\\User Data\\Default"
    
    #print("Home Folder: {0}".format(home_folder))
    
    os.chdir(path) 
    path += "\\History"
    
    copy(path, cwd_path)  # must be copied into antother folder, because it is LOCKED within the .mozilla-folder
    # change the working-directory to where the unlocked sqlite-db is located now
    os.chdir(cwd_path)
    browser_history = []

    try:
        conn = sqlite3.connect(cwd_path + "\\History")
        cursor = conn.cursor()
        print("Connection to database successfully.")
        # open file where all URLs will be stored in
        f = open("URLs_Chrome.txt", "a")
        cursor.execute("select * from urls")
        rows = cursor.fetchall()
        f.write('\n\nQuelle: {0}\n\n'.format(path))
        for row in rows:
            browser_history.append(row[1])
            f.write('\n' + row[1] + '\n')
        cursor.close()
        conn.close()
        f.close()
        print("Connection to database closed.")

    except Exception as err:
        print(err)


def main():
    print('Begin module {0}\n'.format(__name__))
    clean_browser()
    #browser_history_firefox_windows()
    browser_history_chrome_windows()

if __name__ == '__main__':
    main()