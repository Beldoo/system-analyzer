# System Analyzer

This tool allows you to extract, store and visualize/analyze various system parameters.

# Dependencies

The module termcolor must be installed `pip3 install termcolor`
