import re
import sys
import os
from shutil import copy
import re
import sqlite3


def check_platform():

    '''
    This function cheks whether platform is Windows, Linux, MacOS 
    '''

    platform = sys.platform
    return platform


def clean_browser():

    Dir = os.listdir()
    for ii in range(len(Dir)):
        browser_files = re.search('URL|History|places.sqlite', Dir[ii])  # has URL in it
        if (browser_files != None):
            try:
                os.remove(str(Dir[ii]))
                print('\"{0}\" was removed.\n'.format(str(Dir[ii])))
            except:# Exception as err:
                pass
                #print(err)
        



def browser_history_firefox_linux():

    '''
    This function gets the user data (browser history) located in the home directory from Mozilla's Firefox Webbrowser.
    The Program must be installed within the home directory of the user...
    '''

    cwd_path = os.getcwd() # the directory where the database shall be placed (original db is LOCKED, therefore it must be copied)
                           # into this folder
    
    # get the directory-path where the browser data is stored
    homedir = os.environ['HOME']
    path = homedir + '/.mozilla/firefox'
    os.chdir(path) 
    Dir = os.listdir()

    ## retrieve the folder which contains 
    for ii in range(len(Dir)):

        path = homedir + '/.mozilla/firefox'

        data_file = re.search('.default', Dir[ii])  # ending can be ".default" or ".default-release"

        if (data_file != None):

            path = path + '/' + Dir[ii] + '/places.sqlite'  # final path which contains the sqlite-db with the browser history

            copy(path, cwd_path)  # must be copied into antother folder, because it is LOCKED within the .mozilla-folder

            # change the working-directory to where the unlocked sqlite-db is located now
            os.chdir(cwd_path)

            browser_history = []

            try:
                conn = sqlite3.connect(cwd_path + '/places.sqlite')
                cursor = conn.cursor()
                print("Connection to database successfully.")
                # open file where all URLs will be stored in
                f = open("URLs.txt", "a")
                cursor.execute("select * from moz_places")
                rows = cursor.fetchall()
                f.write('\n\nQuelle: {0}\n\n'.format(path))
                for row in rows:
                    browser_history.append(row[1])
                    f.write('\n' + row[1] + '\n')
                cursor.close()
                conn.close()
                f.close()
                print("Connection to database closed.")
            except Exception as err:
                print(err)


def browser_history_chrome_linux():

    '''
    This function gets the user data (browser history) located in the home directory from Google's Chrome Webbrowser.
    The Program must be installed within the home directory of the user...
    '''

    cwd_path = os.getcwd() # the directory where the database shall be placed (original db is LOCKED, therefore it must be copied)
                           # into this folder
    
    # get the directory-path where the browser data is stored
    homedir = os.environ['HOME']
    path = homedir + '/.config/google-chrome/Default/'
    os.chdir(path) 
    #Dir = os.listdir()
    path += '/History'
    
    copy(path, cwd_path)  # must be copied into antother folder, because it is LOCKED within the .mozilla-folder
    # change the working-directory to where the unlocked sqlite-db is located now
    os.chdir(cwd_path)
    browser_history = []

    try:
        conn = sqlite3.connect(cwd_path + '/History')
        cursor = conn.cursor()
        print("Connection to database successfully.")
        # open file where all URLs will be stored in
        f = open("URLs_Chrome.txt", "a")
        cursor.execute("select * from urls")
        rows = cursor.fetchall()
        f.write('\n\nQuelle: {0}\n\n'.format(path))
        for row in rows:
            browser_history.append(row[1])
            f.write('\n' + row[1] + '\n')
        cursor.close()
        conn.close()
        f.close()
        print("Connection to database closed.")

    except Exception as err:
        print(err)

def main():
    print('Beginne..\n')
    clean_browser()
    host_platform = check_platform()  # checks if we deal with Linux/Windows/MacOS and chooses function
    if host_platform == 'linux':
        choice_browser = input("Choose  browser to search:\nFirefox: F\nChrome: C\n")
        if choice_browser == 'F':
            browser_history_firefox_linux()
        elif choice_browser == 'C':
            browser_history_chrome_linux()


if __name__ == '__main__':
    main()