'''
Table containing PID and Process Name
'''

import os
import sys
from termcolor import colored, cprint

processes = []
path = "/proc"

def clean():
    '''
    Delete all files from directory containing file names that are going to be 
    created by the program itself.
    '''

    try:
        os.remove("ps.txt")
    except:
        pass

    try:
        os.remove("error.txt")
    except:
        pass

    try:
        os.remove("names.txt")
    except:
        pass
    
    try:
        os.remove("child_processes.txt")
    except:
        pass

    print("Verzeichnis sauber.\n")



def gather_pids():

    '''
    This function collects all process ids listed in /proc
    '''

    ## PIDs
    for r, d, f in os.walk(path):
        counter = 1
        for item in d:
            #print(item)
            try:
                item = int(item)
                processes.append(item)
            except:
                pass    
        break



def gather_all_infos():

    '''
    For EVERY process all information provided by /proc/<pid>/status are gathered in a text file.
    '''

    for pid in processes:
        #print(pid)
        name_path = path + "/" + str(pid) + "/status"
        #print(name_path)

        myvars = {}

        with open(name_path) as myfile:
            for line in myfile:
                name, var = line.partition(":")[::2]
                myvars[str(name)] = str(var)

                f = open("ps.txt","a")
                f.write("{0} {1} {2}".format(pid, name, myvars[str(name)]))
            f.close()


def output_pid_name():

    '''
    This function filters all the lines containing the process NAMES and associates PIDs, since there are
    many more lines per process conatining additional infos!
    '''

    f = open("ps.txt","r")
    for line in f:
        temp = line.split(" ")
        #print(line.split(" "))
        if "Name" in temp:
            cprint("{0} {1}".format(temp[0], temp[2]), 'blue', attrs=['bold'])


def find_name_of_pid(pid_of_interest):

    '''
    This function takes an integer (=pid) and searches for the associated process name
    ''' 

    f = open("ps.txt","r")
    for line in f:
        temp = line.split(" ")
        if ("Name" in temp) and (temp[0] == str(pid_of_interest)):
            cprint("{0} {1}".format(temp[0], temp[2]), 'blue', attrs=['bold'])
        else:
            pass


def find_pid_of_psname(psname_of_interest):

    '''
    This function takes a string (=process name) and searches for the associated pid
    ''' 

    f = open("ps.txt","r")
    for line in f:
        temp = line.split(" ")
        
        if ("Name" in temp) and (psname_of_interest in temp[2]):
            temp[2] = temp[2].strip('\t')
            cprint("{0} {1}".format(temp[0], temp[2]), 'blue', attrs=['bold'])
            #print(temp)
        else:
            pass



def find_all_children_of_ps(ps):
    '''
    This function takes an integer (= id of certain ps) and find all child processes of this ps
    @INPUT: The Process ID which we want to know all the child processes of
    '''

    child_list = []

    f = open("ps.txt","r")
    for line in f:
        temp = line.split(" ")
        temp[2] = temp[2].strip('\t')
        temp[2] = temp[2].strip('\n')

        if ("PPid" in temp) and (str(ps) == temp[2]):
            child_list.append(temp[0])

    print("Process {0} has {1} child processes.".format(str(ps), len(child_list)))

    save_all = input("Save all child processes? [y|n] ")

    if save_all == "y":
        f = open("./child_processes.txt","w+")
        f.write("Process {0} has the follwing child processes:\n\n".format(str(ps)))
        for child in child_list:
            f.write("{0}\n".format(child))
        f.close()
    else:
        pass
    
    show_graph = input("Show Tree Graph? [y|n] ")
    if show_graph == "y":
        print(str(ps))
        for child in child_list:
            print("|- {0}".format(child))

            
def main():

    cprint("{0} läuft...".format("overview.py"), 'green', 'on_red')
    clean()
    gather_pids()
    gather_all_infos()
    #output_pid_name()
    #find_name_of_pid(1)
    #find_pid_of_psname("systemd")
    find_all_children_of_ps(1)


if __name__=='__main__':
    main()