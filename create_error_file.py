'''
Create error.txt file, in which errors are being logged
'''

def create_error_file():
    f = open("error.txt","w+")
    f.write("List of detected errors:\n")
    f.close()


def main():
    print("create_error_file is running...")
    create_error_file()

if __name__ == '__main__':
    main()